fetch('https://jsonplaceholder.typicode.com/todos/1')
.then((response) => response.json())
.then((json) => console.log(`The item "${json.title}" on the list has a status of false`));

fetch('https://jsonplaceholder.typicode.com/todos')
.then((response) => response.json()) 
.then((json) => {console.log( json.map(post => post.title) )})



fetch("https://jsonplaceholder.typicode.com/todos",{
  method: 'POST',
  headers:{
    'Content-type': 'application/json'
  },
  body: JSON.stringify({
    title: 'Created To Do List Item',  
    completed: false,
    userID:1
    
  })
})
.then((response)=>response.json())
.then((json)=> console.log(json));


fetch("https://jsonplaceholder.typicode.com/todos/1",{
  method: 'PUT',
  headers:{
    'Content-type': 'application/json'
  },
  body: JSON.stringify({
    dataCompleted:"Pending",
    description: "To update the my to do list with a different data structure.",
    id:1,
    status:"Pending",
    title:"Updated to do list item",
    userID:1
  })
})
.then((response)=>response.json())
.then((json)=> console.log(json));

fetch("https://jsonplaceholder.typicode.com/todos/1",{
  method: 'PATCH',
  headers:{
    'Content-type': 'application/json'
  },
  body: JSON.stringify({
    dateCompleted: "07/09/21",
    status: "Complete"
  })
})
.then((response)=>response.json())
.then((json)=> console.log(json));

fetch("https://jsonplaceholder.typicode.com/todos/1",{
  method: 'DELETE',
  
})
.then((response)=>response.json())
.then((json)=> console.log(json))